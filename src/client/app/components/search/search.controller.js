export default class SearchController {
  /* @ngInject */
  constructor($timeout, $element) {
    this.place = {};

    const input = $element[0].querySelector('#search-field');
    const autocomplete = new google.maps.places.Autocomplete(input, {
      types: ['geocode']
    });

    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      $timeout(() => {
        const place = autocomplete.getPlace();
        const formatTedPlaceData = {
          name: place.name,
          fullName: place.formatted_address,
          location: place.geometry.location,
        };
        this.place = formatTedPlaceData;
        this.onSearch(formatTedPlaceData);
      });
    });
  }
}