import angular from 'angular';
import template from './address-list.html';
import controller from './address-list.controller';
import './address-list.css';

export default angular.module('app.address-list', [
  // Empty
]).component('addressList', {
  bindings: {
    list: '<',
    onClick: '&'
  },
  template,
  controller
}).name;