const express = require('express');
require('dotenv').config({ path: __dirname + '/.env' });

const db = require('./db');

const placesRouter = require('./routes/places');
const bodyParser  = require('body-parser');


const app = express();

const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  next();
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(allowCrossDomain);

app.use('/', placesRouter);

app.listen(process.env.PORT, function () {
  console.log(`Example app listening on port ${process.env.PORT}!`);
});
