const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const placeSchema = new Schema({
  name: { type: String, required: true} ,
  fullName: { type: String, required: true} ,
  location: { type: String, required: true }
});

mongoose.model('Place', placeSchema);
