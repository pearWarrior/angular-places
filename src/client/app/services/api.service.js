export class ApiService {
  /*@ngInject*/
  constructor($http) {
    this.$http = $http;
  }

  places() {
    return this.$http.get(`${API_URL}`);
  }

  removePlace(id) {
    return this.$http.delete(`${API_URL}remove/${id}`);
  }

  addPlace(place) {
    return this.$http.post(`${API_URL}add`, place);
  }
}