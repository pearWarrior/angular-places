const mongoose = require('mongoose');
const fs = require('fs');
const join = require('path').join;
const models = join(__dirname, '/models');

mongoose.connect(process.env.DB_URL, { useNewUrlParser: true });
mongoose.Promise = global.Promise;

// Bootstrap models
fs.readdirSync(models)
  .filter(file => ~file.search(/^[^\.].*\.js$/))
  .forEach(file => require(join(models, file)));

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
  console.log('DB is connected');
});


module.exports = db;
