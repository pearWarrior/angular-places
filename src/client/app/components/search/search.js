import angular from 'angular';
import template from './search.html';
import controller from './search.controller';
import GoogleMap from './directives/google-map/google-map';

export default angular.module('app.components', [
  GoogleMap
]).component('search', {
  bindings: {
    onSearch: '&',
    place: '<',
  },
  template,
  controller
}).name;