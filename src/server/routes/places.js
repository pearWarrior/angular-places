const router = require('express').Router();
const { documentToObject } = require('../utils');
const mongoose = require('mongoose');
const Place = mongoose.model('Place');

router.get('/', async(req, res, next) => {
  try {
    const places = await Place.find({});
    res.json(places);
  } catch (error) {
    next(error);
  }
});

router.post('/add', async(req, res, next) => {
  try {
    const place = await Place.create(req.body);
    res.json(place._id);
  } catch (error) {
    next(error);
  }
});

router.delete('/remove/:id', async(req, res, next) => {
  try {
    await Place.findOneAndRemove({ _id: req.params.id });
    res.status(200).send('OK');
  } catch (error) {
    next(error);
  }
});

module.exports = router;