import angular from 'angular';
import template from './app.html';
import controller from './app.controller';
import SearchComponent from './components/search/search';
import AddressListComponent from './components/address-list/address-list';
import Services from './services/index';

import 'noty/lib/noty.css';
import 'noty/lib/themes/mint.css';

import '../style/app.css';
export default angular
  .module('app', [
    SearchComponent,
    AddressListComponent,
    Services
  ])
  .component('app', {
    template,
    controller
  });

