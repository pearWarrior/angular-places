import angular from 'angular';
import Noty from 'noty';
import { ApiService } from './api.service';

export default angular
  .module('app.services', [])
  .service('noty', () => Noty)
  .service('api', ApiService).name;