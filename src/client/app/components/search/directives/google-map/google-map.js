import angular from 'angular';
import './google-map.css';

const ZOOM = {
  INIT: 13,
  ON_PLACE_SHOW: 17
};

export default angular.module('app.directives', [])
  .directive('googleMap', function () {
    return {
      restrict: 'E',
      scope: {
        place: '<'
      },
      link: (scope, element) => {
        const latLng = new google.maps.LatLng(scope.lat, scope.leng);

        const mapOptions = {
          center: latLng,
          zoom: ZOOM.INIT,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        const map = new google.maps.Map(element[0].querySelector('#map'), mapOptions);
        const marker = new google.maps.Marker({
          position: latLng,
          map: map
        });
        const infoWindow = new google.maps.InfoWindow();

        const unWatch = scope.$watch('place', (place) => {
          if (!place) {
            return;
          }

          map.setCenter(place.location);
          map.setZoom(ZOOM.ON_PLACE_SHOW);

          moveMarker(place.name, place.location);
        });

        function moveMarker(placeName, newLatlng) {
          marker.setPosition(newLatlng);

          if (!placeName || placeName === '') {
            return infoWindow.close();
          }
          infoWindow.setContent(placeName);
          infoWindow.open(map, marker);
        }

        scope.$on('$destroy', function () {
          unWatch();
        });
      },
      template: '<div class="container"><div id="map"></div></div>'
    };
  }).name;