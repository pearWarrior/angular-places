export default class AppController {
  /* @ngInject */
  constructor(noty, api) {
    this.address = [];
    api.places()
      .then(({ data }) => {
        this.address = data;
      })
      .catch(() => {
        new noty({
          text: 'Connect to server failed. Visits are not saved.',
          type: 'error'
        }).show();
      });

    this.api = api;
    this.place = {
      location: {
        lat: Number(LAT),
        lng: Number(LENG)
      }
    };
  }

  addAddress(name, fullName, location) {
    const place = { name, fullName, location };
    this.address.push(place);
    this.api.addPlace({ name, fullName, location: JSON.stringify(place.location) })
      .then(({ data }) => {
        place._id = data;
      });
  }

  onClick(place) {
    this.place = place;
  }
}