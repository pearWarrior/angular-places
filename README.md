**REQUIRED**
* nodejs 8+
* mongodb(front-end works without db, but history not save)

```
# clone our repo
$ git clone git@bitbucket.org:pearWarrior/angular-places.git my-app

# change directory to your app
$ cd my-app

# clone envs
cp src/client/.env.example src/client/.env
cp src/server/.env.example src/server/.env

# install the dependencies with npm
$ npm install

# start the client-server
$ npm start

# start the node-server
$ npm run node-server

http://localhost:8080/
```