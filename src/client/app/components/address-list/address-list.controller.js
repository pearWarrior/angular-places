export default class AddressListController {
  constructor(noty, $timeout, api) {
    /** bindings */
    this.list = [];
    this.noty = noty;
    this.api = api;
    this.$timeout = $timeout;
  }

  onDelete(index, id) {
    const notyInstance = new this.noty({
      text: 'Do you really want to delete?',
      modal: true,
      buttons: [
        this.noty.button('YES', 'btn btn-success', () => {
          this.$timeout(() => this.list.splice(index, 1));
          this.api.removePlace(id);
          notyInstance.close();
        }, { id: 'button1', 'data-status': 'yes' }),

        this.noty.button('NO', 'btn btn-error', () => {
          notyInstance.close();
        })
      ],
      type: 'alert'
    }).show();
  }
}